import { Cliente } from './cliente.model'
import { Empleado } from './empleado.model';

export class Asistencia {
    cliente : Cliente; 
    fecha : Date;
    empleado : Empleado; 
}