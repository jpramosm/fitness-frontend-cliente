export class Cliente{
    _id:string;
    nombre:string;
    apellidos:string;
    telefono:string;
    correo:string;
    password:string;
    nacimiento:Date;
    codigoRegistro:string;
    genero:Number;
    imagen:string;
    estado:string;
    dni : string ;
    tipoRegistro : string ;
}


