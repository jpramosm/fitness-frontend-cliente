import { Cliente } from './cliente.model'
import { Plan } from './plan.model';
import { Cupon } from './cupon.model';

export class Matricula {
    
    cliente : Cliente;
    plan: Plan;
    fecha: Date;
    fechaInicio: Date;
    fechaFin: Date;
    total: number;
    cupon: Cupon;
    estado: string;
    pago: number;
    estadoPago: Boolean;

}