export class Cupon {
    _id: string;
    nombre: string;
    descuento: number;
    estado: boolean;
}