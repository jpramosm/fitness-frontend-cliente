export class Empleado{
    _id: string ;
    nombre : string;
    apellidos : string;
    dni : string;
    telefono : string;
    correo : string;
    password : string;
    direccion : string;
    cargo : string;
    estado : boolean;
    imagen : string ;
    
}