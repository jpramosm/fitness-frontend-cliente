import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config/config';
import { Observable } from 'rxjs';
import { Plan } from '../models/plan.model';


@Injectable({
    providedIn:'root'
})
export class PlanService {

    constructor(private http:HttpClient,private config:ConfigService){}

    listarPlanes():Observable<Plan[]>{
        return this.http.get<Plan[]>(`${this.config.urlService}plan`);
    }

    detallePlan(idPlan:string):Observable<Plan>{
        return this.http.get<Plan>(`${this.config.urlService}plan/${idPlan}`);
    }

}