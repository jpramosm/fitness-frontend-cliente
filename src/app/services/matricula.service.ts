import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config/config';
import { Matricula } from '../models/matricula.model';
import { Observable } from 'rxjs';


@Injectable({
    providedIn:'root'
})
export class MatriculaService {
    
    public matriculaSeleccioanda : Matricula = new Matricula() ;

    constructor(private http:HttpClient,private config : ConfigService){}

    registrarMatricula(matricula:Matricula):Observable<Matricula>{
        return this.http.post<Matricula>(`${this.config.urlService}matricula`, matricula);
    }

    listarMatriculas(idcliente:String):Observable<Matricula[]>{
        return this.http.get<Matricula[]>(`${this.config.urlService}matricula/cliente/${idcliente}`);
    }



}