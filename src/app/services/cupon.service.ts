import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config/config';
import { Observable } from 'rxjs';
import { Cupon } from '../models/cupon.model';


@Injectable({
    providedIn : 'root'
})
export class CuponService {
    constructor(private http:HttpClient, private config:ConfigService){}
    
    obtenerCupon(nombre:string):Observable<Cupon>{
        return this.http.get<Cupon>(`${this.config.urlService}cupon/${nombre}`);
    }

    listarCupones(): Observable<Cupon[]> {
        return this.http.get<Cupon[]>(`${this.config.urlService}cupon`);
    }
    
}