import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config/config';
import { Observable } from 'rxjs';
import { Asistencia } from '../models/asistencia.model';
import { UsuarioService } from './usuario.service';


@Injectable({
    providedIn: 'root'
})
export class AsistenciaService {

    public fecha1 : Date = null;
    public fecha2 : Date = null;
    
    constructor(private http : HttpClient,private config:ConfigService,private clienteService:UsuarioService){}

    listarAsistenciaMatricula(): Observable<Asistencia[]> {
        return this.http.get<Asistencia[]>(`${this.config.urlService}asistencia/cliente/${this.fecha1}/${this.fecha2}/${this.clienteService.cliente._id}`)
    };


}