import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cliente } from '../models/cliente.model';
import { ConfigService } from '../config/config';


@Injectable({
    providedIn: 'root'
})

export class UsuarioService {

    public cliente  :Cliente = null;

    constructor(private http: HttpClient, private config: ConfigService) {}

    registrarUsuario(usuario: Cliente): Observable<Cliente> {
        return this.http.post<Cliente>(`${this.config.urlService}cliente`, usuario);
    }

    actualizarUsuario(usuario:Cliente):Observable<Cliente>{
        return this.http.put<Cliente>(`${this.config.urlService}cliente/${usuario._id}`, usuario);
    }

    loginUsuario(correo: string, password: string): Observable<Cliente> {
        return this.http.post<Cliente>(`${this.config.urlService}cliente/login`, { correo, password });
    }

    loginUsuarioSocial(usuario: Cliente): Observable<Cliente> {
        return this.http.post<Cliente>(`${this.config.urlService}cliente/loginsocial`, usuario);
    }

    cambiarFoto(imagen: File, idusuario: string): Observable<Cliente> {
        const form = new FormData();
        form.append('image', imagen);
        return this.http.put<Cliente>(`${this.config.urlService}cliente/imagen/${idusuario}`, form);
    }

    cambiarCorreo(correo: string, password: string): Observable<Cliente> {
        return this.http.put<Cliente>(`${this.config.urlService}cliente/correo/${this.cliente._id}`, { correo, password });
    }

    enviarCorreoVerificacion(cliente:Cliente){
        return this.http.put(`${this.config.urlService}cliente/enviarCorreo/correo`,cliente);
    }

    guaradarClienteLocalStorage(cliente: Cliente) {
        if (localStorage.getItem('cliente')) {
            localStorage.removeItem('cliente');
            localStorage.setItem('cliente', JSON.stringify(cliente));
            this.cliente = cliente;
        } else {
            this.cliente = cliente;
            localStorage.setItem('cliente', JSON.stringify(cliente));
        }
    }



}