import { Component, OnDestroy, OnInit } from '@angular/core';
import { UsuarioService } from './services/usuario.service';
declare let $:any; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  constructor(public usuarioService:UsuarioService){
    let cliente =  JSON.parse(localStorage.getItem('cliente'));
    if (cliente) {
      this.usuarioService.cliente = cliente; 
    }
  }
  
  ngOnInit(): void {
    
    
  }
  
}
