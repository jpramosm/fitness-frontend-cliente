import { Injectable } from '@angular/core';
import Swal from 'sweetalert2'

@Injectable({
    providedIn: 'root'
})

export class AlertService {
    constructor(){}

    error(titulo:string,mensaje:string){
        return Swal.fire(titulo,mensaje,'error');
    }

    success(titulo:string,mensaje:string){
        return Swal.fire(titulo,mensaje,'success');
    }

    question(titulo:string,mensaje:string){
        return Swal.fire({
            title: titulo,
            text: mensaje,
            icon: 'question',
            background: '#000000',
            showCancelButton: true,
            confirmButtonColor: '#000000',
            cancelButtonColor: '#000000',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        });
    }
}