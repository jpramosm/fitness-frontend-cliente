import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";


import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { HomeComponent } from './components/pages/home/home.component';
import { InicioSesionComponent } from './components/pages/login/inicio-sesion/inicio-sesion.component';
import { RegistrarComponent } from './components/pages/login/registrar/registrar.component';
import { UsuarioService } from './services/usuario.service';
import { APP_ROUTES } from './app.routes';
import { PlanesComponent } from './components/pages/planes/planes.component';
import { PlanDetalleComponent } from './components/pages/plan-detalle/plan-detalle.component';
import { LoadingComponent } from './components/shared/loading/loading.component';
import { ProfileComponent } from './components/pages/profile/profile.component';
import { MatriculaDetalleComponent } from './components/pages/matricula-detalle/matricula-detalle.component';
import { NosotrosComponent } from './components/pages/nosotros/nosotros.component';
import { ContactanosComponent } from './components/pages/contactanos/contactanos.component';
import { CorreoCambiarComponent } from './components/pages/correo-cambiar/correo-cambiar.component';
import { PagesComponent } from './components/pages/pages.component';
import { CorreoVerificarComponent } from './components/pages/correo-verificar/correo-verificar.component';
import { AgregarDatosComponent } from './components/pages/agregar-datos/agregar-datos.component';
import { CorreoAgregarComponent } from './components/pages/correo-agregar/correo-agregar.component';
import { AsistenciaListarComponent } from './components/pages/asistencia-listar/asistencia-listar.component';


let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("277834400165-ankmp502h47h49i316jbgsk14ue26rjm.apps.googleusercontent.com")
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("442237546661457")
  }
]);

export function provideConfig() {
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    InicioSesionComponent,
    RegistrarComponent,
    PlanesComponent,
    PlanDetalleComponent,
    LoadingComponent,
    ProfileComponent,
    MatriculaDetalleComponent,
    NosotrosComponent,
    ContactanosComponent,
    CorreoCambiarComponent,
    PagesComponent,
    CorreoVerificarComponent,
    AgregarDatosComponent,
    CorreoAgregarComponent,
    AsistenciaListarComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    APP_ROUTES,
    SocialLoginModule
  ],
  entryComponents: [
    InicioSesionComponent ,
    MatriculaDetalleComponent,
    CorreoCambiarComponent,
    AgregarDatosComponent,
    CorreoAgregarComponent,
    AsistenciaListarComponent
  ],
  providers: [
    UsuarioService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
