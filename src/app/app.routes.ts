import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/pages/home/home.component';
import { PlanesComponent } from './components/pages/planes/planes.component';
import { PlanDetalleComponent } from './components/pages/plan-detalle/plan-detalle.component';
import { ProfileComponent } from './components/pages/profile/profile.component';
import { AuthGuard } from './guards/auth.guard';
import { confirmEmail } from './guards/confirmEmail.guard';
import { PagesComponent } from './components/pages/pages.component';
import { CorreoVerificarComponent } from './components/pages/correo-verificar/correo-verificar.component';
import { NosotrosComponent } from './components/pages/nosotros/nosotros.component';
import { ContactanosComponent } from './components/pages/contactanos/contactanos.component';



const routes: Routes = [
    
    {
        path: '',
        component: PagesComponent,
        children: [
            { path: 'home', component: HomeComponent },
            { path: 'about', component: NosotrosComponent },
            { path: 'planes', component: PlanesComponent },
            { path: 'contact', component: ContactanosComponent },
            { path: 'plan/:id', component: PlanDetalleComponent, canActivate: [AuthGuard, confirmEmail ] },
            { path: 'perfil/:id', component: ProfileComponent, canActivate: [AuthGuard, confirmEmail] },
            { path: '', redirectTo: '/home', pathMatch: 'full' },
        ]
    },
    {path:'verificarcorreo',component: CorreoVerificarComponent}

];


export const APP_ROUTES = RouterModule.forRoot(routes, { useHash: true });
