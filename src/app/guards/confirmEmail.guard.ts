import { Injectable } from '@angular/core';
import { CanActivate, Router  } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';
import { AlertService } from '../config/alerts';

@Injectable({
  providedIn: 'root'
})
export class confirmEmail implements CanActivate {

  constructor(private usuarioService:UsuarioService, private router:Router , private alert : AlertService){}

  canActivate() {
    if (this.usuarioService.cliente.estado !== 'Registrado') {
        return true;
    } else {
      this.router.navigate(['/home']);
      this.alert.error('Correo no confirmado', 'Porfavor revise su bandeja de entrada y confirme su correo');
      return false;
    }
  }

}
