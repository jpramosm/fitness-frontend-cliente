import { Injectable } from '@angular/core';
import { CanActivate, Router  } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';
import { AlertService } from '../config/alerts';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private usuarioService:UsuarioService, private router:Router , private alert : AlertService){}

  canActivate() {
    if (this.usuarioService.cliente) {
        return true;
    } else {
      this.router.navigate(['/home']);
      this.alert.error('Inicio de sesión requerido', 'Nesecita estar logeado para acceder a la pagina selecionada');
      return false;
    }
  }

}
