import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InicioSesionComponent } from '../../pages/login/inicio-sesion/inicio-sesion.component';
import { UsuarioService } from 'src/app/services/usuario.service';
import { AlertService } from 'src/app/config/alerts';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent implements OnInit {

  constructor(private modal:NgbModal, public usuarioService:UsuarioService ,private alert:AlertService, private router:Router) { }

  ngOnInit() {
  }

  openLogin(){
    window.scrollTo(0,0);
    this.modal.open(InicioSesionComponent)
  }

  cerrarSesion() {
    this.alert.question('Cerrar Sesión?', 'Presione cancel si no esta deacuerdo')
      .then(res => {
        if (res.value) {
          this.usuarioService.cliente = null;
          localStorage.removeItem('cliente');
          this.alert.success('Sesión cerrada', 'Gracias por su visita vuelva pronto');
          this.router.navigate(['/home']);
        }
      })
    
  }

}
