import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { AlertService } from 'src/app/config/alerts';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-correo-cambiar',
  templateUrl: './correo-cambiar.component.html',
  styles: []
})
export class CorreoCambiarComponent implements OnInit {

  
  correo : string ;
  password : string ;
  password2 : string ;

  constructor(private usuarioService:UsuarioService,private alert: AlertService,public activeModal:NgbActiveModal) { }

  ngOnInit() {
  }

  cambiarCorreo() {
    if (this.password === this.password2) {
      this.usuarioService.cambiarCorreo(this.correo, this.password)
        .subscribe(res => console.log(res),
          err => console.log(err))
    } else {
      this.alert.error('Las contraseñas no coinciden', 'Por favor verfique que las contraseñas sean iguales');
    }
  }

}
