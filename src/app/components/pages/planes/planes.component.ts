import { Component, OnInit } from '@angular/core';
import { PlanService } from 'src/app/services/plan.service';
import { Plan } from 'src/app/models/plan.model';

@Component({
  selector: 'app-planes',
  templateUrl: './planes.component.html',
  styles: []
})
export class PlanesComponent implements OnInit {

  planes:Plan[];

  constructor(private planService:PlanService) { }

  ngOnInit() {
    this.listarPlanes();
  }

  listarPlanes(){
    this.planService.listarPlanes()
      .subscribe(res=> this.planes = res,
      err => console.log(err))
  }

}
