import { Component, OnInit } from '@angular/core';
import { Plan } from 'src/app/models/plan.model';
import { PlanService } from 'src/app/services/plan.service';
import { CuponService } from 'src/app/services/cupon.service';
import { Cupon } from 'src/app/models/cupon.model';

declare let $ : any ;
declare let jQuery : any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  planes:Plan[];
  cupon : Cupon ;

  constructor(private planService:PlanService,private cuponService : CuponService) { }

  ngOnInit() {
    this.listarPlanes();
    this.listarCupones();
  }

  listarPlanes(){
    this.planService.listarPlanes()
      .subscribe(res=> this.planes = res,
      err => console.log(err))
  }

  listarCupones(){
    this.cuponService.listarCupones()
      .subscribe(res => {
        console.log(res);
        if (res[res.length - 1].estado == true) {
          this.cupon = res[res.length - 1]
        }else{
          this.cupon = null;
        }
      }, err => {
        console.log(err);
      })
  }

}
