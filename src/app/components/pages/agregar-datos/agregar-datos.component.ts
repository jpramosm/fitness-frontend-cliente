import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/models/cliente.model';
import { UsuarioService } from 'src/app/services/usuario.service';
import { AlertService } from 'src/app/config/alerts';
import { NgForm } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-agregar-datos',
  templateUrl: './agregar-datos.component.html',
  styles: []
})
export class AgregarDatosComponent implements OnInit {


  cliente : Cliente = new Cliente();

  constructor(private clienteService : UsuarioService, private alert : AlertService,public activeModal: NgbActiveModal ) { }

  ngOnInit() {
  }

  calculateAge(birthday) { // birthday is a date
    var ageDifMs = Date.now() -  new Date(this.cliente.nacimiento).getTime()  ;
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }
 

  agregardatos(form:NgForm) {

    let años   =  this.calculateAge(this.cliente.nacimiento);
    
    if ( new Date(this.cliente.nacimiento)  >= new Date() ) return this.alert.error('Edad no valida','La Fecha ingresada no es valida');
    if ( años < 16 ) return this.alert.error('Edad no valida','Debes de ser mayor de 16 años para obtener una matricula');
      
    if (!form.valid) {
      return this.alert.error('Datos no validos','Verfique no sean erroneos o esten vacios') 
    }else{
      return this.actualizar();
    }    
    
  }


  actualizar() {
    this.cliente._id = this.clienteService.cliente._id;
    this.clienteService.actualizarUsuario(this.cliente)
      .subscribe(res => {
        this.alert.success('Realizado', 'Sus Datos han sido actualizados');
        this.clienteService.guaradarClienteLocalStorage(res);
        this.activeModal.close();
      }, err => {
          this.alert.error('Ups! algo salio mal', 'Intentelo nuevamente');
        }
      )
  }

}
