import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/config/alerts';

@Component({
  selector: 'app-contactanos',
  templateUrl: './contactanos.component.html',
  styles: []
})
export class ContactanosComponent implements OnInit {

  constructor(private alertS : AlertService) { }

  ngOnInit() {
  }

  enviarCorreo(){
    this.alertS.success('Realizado','Correo enviado');
  }

}
