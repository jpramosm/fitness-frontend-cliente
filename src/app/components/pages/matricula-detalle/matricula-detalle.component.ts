import { Component, OnInit } from '@angular/core';
import { MatriculaService } from 'src/app/services/matricula.service';
import { Matricula } from 'src/app/models/matricula.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-matricula-detalle',
  templateUrl: './matricula-detalle.component.html',
  styles: []
})
export class MatriculaDetalleComponent implements OnInit {

  matricula : Matricula = new Matricula();
  diasRestantes : number; 
  fecha : Date = new Date();
  days : number ;

  constructor(private matriculaService:MatriculaService,public activeModal: NgbActiveModal) {
       this.matricula =  this.matriculaService.matriculaSeleccioanda 
   }

  ngOnInit() {
    this.matricula.fechaFin = new Date(this.matricula.fechaFin)
    if (this.matricula.fechaFin) {
      var timeDiff = Math.abs(this.matricula.fechaFin.getTime() - this.fecha.getTime());
      this.days = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
    }
    
  }

  closeModal(){
    this.activeModal.close();
  }

}
