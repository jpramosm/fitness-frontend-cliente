import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/models/cliente.model';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/config/alerts';
import { UsuarioService } from 'src/app/services/usuario.service';
import { NgbModal ,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-correo-agregar',
  templateUrl: './correo-agregar.component.html',
  styles: []
})
export class CorreoAgregarComponent implements OnInit {


  cliente :Cliente = new Cliente();
  password2 : string ;

  constructor(private alert : AlertService , private clienteService: UsuarioService,public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  validarRegistro(form:NgForm){

    if (this.cliente.password !== this.password2) return  this.alert.error('Error','Las contraseñas no coinciden');

    if (form.valid) {
      this.actualizar();
    }else{
      this.alert.error('Error','Verifique que los datos sean correctos');
    }

  }

  actualizar() {
    this.cliente.estado = 'Registrado';
    this.cliente._id = this.clienteService.cliente._id;
    this.clienteService.actualizarUsuario(this.cliente)
      .subscribe(res => {
        this.clienteService.guaradarClienteLocalStorage(res);
        this.alert.success('Correo Registrado', 'Revise su bandeja de entrada y verifique su correo');
        this.activeModal.close();
      }, err => {
        if (err.error.codeName == 'DuplicateKey') {
          return this.alert.error('Correo invalido', 'El correo ingresado ya esta en uso ingrese otro');
        } else {
          return this.alert.error('Ups algo salio mal!!', 'Intentanlo nuevamente si el error persiste comuniquese con el adminsitrador');
        }
      })
  }

}
