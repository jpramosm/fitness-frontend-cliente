import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlanService } from 'src/app/services/plan.service';
import { Plan } from 'src/app/models/plan.model';
import { CuponService } from 'src/app/services/cupon.service';
import { Cupon } from 'src/app/models/cupon.model';
import { AlertService } from 'src/app/config/alerts';
import { isNullOrUndefined } from 'util';
import { MatriculaService } from 'src/app/services/matricula.service';
import { Matricula } from 'src/app/models/matricula.model';
import { UsuarioService } from 'src/app/services/usuario.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AgregarDatosComponent } from '../agregar-datos/agregar-datos.component';

@Component({
  selector: 'app-plan-detalle',
  templateUrl: './plan-detalle.component.html',
  styles: []
})
export class PlanDetalleComponent implements OnInit {

  id: string;
  handler: StripeCheckoutHandler;
  plan: Plan = new Plan();
  cupon: Cupon = new Cupon();
  descuento: number;
  total: number;
  matricula: Matricula;

  constructor(private route: ActivatedRoute,
    private planService: PlanService,
    private cuponService: CuponService,
    private alert: AlertService,
    private matriculaService: MatriculaService,
    private usuarioService: UsuarioService,
    private router : Router,
    private modal:NgbModal 
  ) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.detallesPlan();
  }

  ngOnInit() {
    this.loadStripe();
  }

  detallesPlan() {
    this.planService.detallePlan(this.id)
      .subscribe(res => { this.plan = res; this.total = res.precio },
        err => console.log(err))
  }

  ingresarCupon(nombre: string) {
    if (nombre == '') { return this.alert.error('Error', 'Ingrese un cupon valido') }
    this.cuponService.obtenerCupon(nombre)
      .subscribe(res => {
        if (isNullOrUndefined(res)) {
          this.alert.error('Error', 'Cupon no encontrado');
        } else {
          this.cupon = res;
          this.alert.success(`Cupon (${this.cupon.nombre}) aplicado `, `Descuento ${this.cupon.descuento}%`);
          this.descuento = this.total * (this.cupon.descuento / 100);
          this.total = this.total - this.descuento
        }
      },
        err => this.alert.error('Error', 'Ups! algo salio mal intentalo nuevamente'))
  }


  registrarMatricula() {
    
    this.matricula = ({
      cupon: this.cupon,
      cliente : this.usuarioService.cliente,
      plan: this.plan,
      fecha: new Date(),
      estado: 'matriculado',
      total: this.total,
      fechaFin: null,
      fechaInicio: null,
      estadoPago: true,
      pago: this.total
    })
    this.matriculaService.registrarMatricula(this.matricula)
      .subscribe(res => {
        console.log(res);
        this.alert.success('Realizado','La matricula fue registradda');
        this.router.navigateByUrl(`/perfil/${this.usuarioService.cliente._id}`)
      },
        err => console.log(err))
  }

  loadStripe() {
    if (!window.document.getElementById('stripe-script')) {
      var s = window.document.createElement("script");
      s.id = "stripe-script";
      s.type = "text/javascript";
      s.src = "https://checkout.stripe.com/checkout.js";
      window.document.body.appendChild(s);
    }
  }

  pay(amount) {
  
    this.handler = StripeCheckout.configure({
      key: 'pk_test_rddHKCMGk0zwl3mo40nmft9Z009z3eQGH0',
      locale: 'auto',
      image: 'https://res.cloudinary.com/continental/image/upload/v1574456238/logo_q3k6qa.jpg',
      closed: () => {
        console.log('Pago Cancelado');
      },
      token: async (token) => {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
        await this.registrarMatricula();
        console.log(token.id);
      }
    });

    this.handler.open({
      name: 'Matricula',
      description: `Inscripcion por ${this.plan.cantidadMeses} meses`,
      currency: 'PEN',
      amount: amount * 100
    });

  }

  agregarDatos(){
    window.scrollTo(0,0);
    this.modal.open(AgregarDatosComponent);
  }


  verificarCliente() {
    this.matriculaService.listarMatriculas(this.usuarioService.cliente._id)
      .subscribe(res => {
        for (const i of res) {
          if (i.estado !== 'finalizado') {
            return this.alert.error('Error', 'Actualmente tiene una matricula activa, espere que esta haya finalizado');
          }
        }
        if (!this.usuarioService.cliente.nacimiento) {
          this.alert.question('Fecha de nacimiento no agregada', 'Porfavor actualize sus datos')
            .then(res => {
              if (res.value) {
                return this.agregarDatos();
              } else {
                return;
              }
            })
        } else {
          this.pay(this.total);
        }
      },
        err => console.log(err))
  }



}
