import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { AlertService } from 'src/app/config/alerts';
import { NgbModal ,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { Cliente } from 'src/app/models/cliente.model';



@Component({
  selector: 'app-inicio-sesion',
  templateUrl: './inicio-sesion.component.html',
  styles: []
})
export class InicioSesionComponent implements OnInit {

  existe : boolean = true ;
  correo : string ;
  password : string ;
  usuario : Cliente = new Cliente(); 

  constructor(private usuarioService:UsuarioService,
    private alert:AlertService,
    public activeModal: NgbActiveModal,
    private authService: AuthService ) { }

  ngOnInit() {
  }

  login() {
    this.usuarioService.loginUsuario(this.correo, this.password)
      .subscribe(res => this.logincorrecto(res),
        err => this.alert.error('Error', 'Verfique que el usuario o la contraseña sean los correctos'))
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
      .then(res => {
        this.usuario.nombre = res.firstName;
        this.usuario.apellidos = res.lastName;
        this.usuario.imagen = res.photoUrl;
        this.usuario.correo = res.email;
        this.usuario.tipoRegistro = 'Google';
        this.loginsocial();
      })
      .catch(err => this.alert.error('Error!!','Algo salio mal intentelo nuevamente'))
  }

  loginsocial() {
    this.usuarioService.loginUsuarioSocial(this.usuario)
      .subscribe(res => {
        if (res.tipoRegistro == 'Normal') {
          this.alert.error('Login incorrecto!!', 'Porfavor utlize el login comun');
        } else {
          this.logincorrecto(res)
        }

      },
        err => console.log(err))
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID)
      .then(res => {
        this.usuario.nombre = res.firstName;
        this.usuario.apellidos = res.lastName;
        this.usuario.imagen = res.photoUrl;
        this.usuario.correo = res.email;
        this.usuario.tipoRegistro = 'Facebook';
        this.loginsocial();
      })
      .catch(err => this.alert.error('Error!!','Algo salio mal intentelo nuevamente'))
  } 

  logincorrecto(cliente:Cliente) {
    this.alert.success(`Bienvenido ${cliente.nombre}`, 'Inicio de sesión correcto');
    this.usuarioService.cliente = cliente;
    this.activeModal.close()
    localStorage.setItem('cliente', JSON.stringify(cliente));
  }


}
