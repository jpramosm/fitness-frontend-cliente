import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/models/cliente.model';
import { NgForm } from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuario.service';
import { AlertService } from 'src/app/config/alerts';
import { NgbModal ,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styles: []
})
export class RegistrarComponent implements OnInit {

  usuario : Cliente = new Cliente();
  
  password2 : string ;

  constructor(private usuarioService:UsuarioService, private alert:AlertService,public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  validarRegistro(formulario: NgForm) {
    if (formulario.valid == true && this.usuario.password === this.password2) {
      this.alert.question('Finalizar registro?', 'Si los datos datos ingresados son correctos presione OK')
        .then(res => {
          if (res.value) {
            this.registrarUsuario();
          }
        });
    } else {
      this.alert.error('Datos incorrectos', 'Los datos ingresados son erroneos o estan incompletos');
    }
  }

  registrarUsuario() {
    this.usuario.estado = 'Registrado';
    this.usuario.tipoRegistro = 'Normal';
    this.usuarioService.registrarUsuario(this.usuario)
      .subscribe(res => {
        this.alert.success('Participante Registrado', 'Se han revise su bandeja de entrada y verifique su correo');
        this.activeModal.close();
      },
        err => {
          console.log(err)
          if (err.error.errors.correo) {
            return this.alert.error('Correo invalido', 'El correo ingresado ya esta en uso ingrese otro');
          } else {
            return this.alert.error('Ups algo salio mal!!', 'Intentanlo nuevamente si el error persiste comuniquese con el adminsitrador');
          }
        })
  }



}
