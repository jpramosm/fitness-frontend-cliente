import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AsistenciaService } from 'src/app/services/asistencia.service';
import { Asistencia } from 'src/app/models/asistencia.model';

@Component({
  selector: 'app-asistencia-listar',
  templateUrl: './asistencia-listar.component.html',
  styleUrls: []
})

export class AsistenciaListarComponent implements OnInit {

  
  asistencias : Asistencia[];

  constructor(public activeModal: NgbActiveModal, private asistenciaService:AsistenciaService) { }


  ngOnInit() {
    this.listarAsistencia();
  }

  listarAsistencia() {
    if (!this.asistenciaService.fecha1) return;
    this.asistenciaService.listarAsistenciaMatricula()
      .subscribe(res => {this.asistencias = res;console.log(res);},
        err => console.log(err))
  }


}
