import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Cliente } from 'src/app/models/cliente.model';
import { MatriculaService } from 'src/app/services/matricula.service';
import { Matricula } from 'src/app/models/matricula.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatriculaDetalleComponent } from '../matricula-detalle/matricula-detalle.component';
import { AlertService } from 'src/app/config/alerts';
import { AgregarDatosComponent } from '../agregar-datos/agregar-datos.component';
import { CorreoAgregarComponent } from '../correo-agregar/correo-agregar.component';
import { AsistenciaService } from 'src/app/services/asistencia.service';
import { AsistenciaListarComponent } from '../asistencia-listar/asistencia-listar.component';

interface HtmInputEvent extends Event{
    target: HTMLInputElement & EventTarget;
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  matriculas : Matricula[];
  usuario:Cliente;
  fechaNacimineto:any;
  imagen : File ;
  imagenvista : ArrayBuffer | string ;

  constructor(public usuarioService:UsuarioService, 
    private matriculadoService:MatriculaService , 
    private modal:NgbModal ,
    private alert : AlertService,
    private asistenciaService : AsistenciaService) {
    this.usuario = usuarioService.cliente;
    this.listarMatriculas();
   }

  ngOnInit() {
  }

  listarMatriculas(): void {
    this.matriculadoService.listarMatriculas(this.usuario._id)
      .subscribe(res => {
        this.matriculas = res;
        console.log(res);
      },
        err => console.log(err))
  }

  verMatricula(matricula:Matricula){
    window.scrollTo(0,0);
    this.matriculadoService.matriculaSeleccioanda = matricula;
    this.modal.open(MatriculaDetalleComponent);
  }

  verAsistencias(fecha1: Date, fecha2: Date) {
    window.scrollTo(0, 0);
    this.asistenciaService.fecha1 = fecha1;
    this.asistenciaService.fecha2 = fecha2;
    this.modal.open(AsistenciaListarComponent);
  }


  agregarDatos(){
    window.scrollTo(0,0);
    this.modal.open(AgregarDatosComponent);
  }

  seleccionarFoto(event : HtmInputEvent  ) : void {
    if (event.target.files && event.target.files[0]  ) {
      this.imagen = event.target.files[0];
      const reader = new FileReader();
      reader.onload = e => this.imagenvista = reader.result;
      reader.readAsDataURL(this.imagen);
    }
  }

  enviarCorreo() {
    this.usuarioService.enviarCorreoVerificacion(this.usuarioService.cliente)
      .subscribe(res => this.alert.success('Correo reenviado','Por favor verifique su bandeja de entrada'),
        err => console.log(err))
  }

  agregarCorreo(){
    this.modal.open(CorreoAgregarComponent);
  }

  subirfoto() {
    this.usuarioService.cambiarFoto(this.imagen, this.usuarioService.cliente._id)
      .subscribe(res => {
        this.alert.success('Foto Actualizada','Imagen de perfil cambiada con exito!!');
        
      },
        err => console.log(err))
  }


}
