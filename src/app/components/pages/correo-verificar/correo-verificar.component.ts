import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { AlertService } from 'src/app/config/alerts';
import { Cliente } from 'src/app/models/cliente.model';

@Component({
  selector: 'app-correo-verificar',
  templateUrl: './correo-verificar.component.html',
  styles: []
})
export class CorreoVerificarComponent implements OnInit {

  correo : string ;
  password : string;

  constructor(private usuarioService:UsuarioService, private alert:AlertService ) { }

  ngOnInit() {
  }


  verificar() {
    console.log(this.password,this.correo);
    this.usuarioService.loginUsuario(this.correo, this.password)
      .subscribe(res => {
        this.alert.success(`Enhorabuena ${res.nombre}`,'Tu correo ha sido verficado');
        this.actualizarEstado(res);
      },err => {
        this.alert.error('Error','Verfique que el usuario o la contraseña sean los correctos');
      })
  }

  actualizarEstado(usuario: Cliente) {
    usuario.estado = 'verificado';
    delete usuario.password;
    this.usuarioService.actualizarUsuario(usuario)
      .subscribe(res => console.log(res),
        err => console.log(err))
  }

}
